\documentclass[../main/shgo.tex]{subfiles}
\begin{document}

\section{Experimental Results} \label{sec:results}
\subsection{Invariance and optimum minimiser pool}
The following 4 optimisation test problems were used to demonstrate the applications of \autoref{theorem:invariance_M} and to show the minimiser pool growth compared to TGO over a large number of sampling points. The results plotted \autoref{fig:resmin} shows that SHGO performed as expected with the minimiser pool staying at the optimum cardinality to map all the local minima once the sampling is adequate as well as the shortcomings of the TGO especially in the higher dimensional test problems where the the minimiser pool tends to grow rapidly with the number sampling points $N$.

The Ursem01 function for two dimensions is defined as follows \cite{Gavana2016}
\begin{equation} \label{eq:Ursem01}
\min f(\bold{x}) =  \displaystyle - \sin{\left(2 x_1  - 0.5 \pi \right) - 3 \cos{\left(x_2\right)}} - 0.5 x_1 , ~ \bold{x} \in [0, 9] \times [-2, 2] \subset \mathbb{R}^2
\end{equation}

The Paraboloid function for six dimensions is defined as follows %\cite{Gavana2016}
\begin{equation} \label{eq:Paraboloid }
\min f(\bold{x}) =  \displaystyle \sum^6_{i=1} x_i , ~ \bold{x} \in [-10, 10]^6 \subset \mathbb{R}^6
\end{equation}

The Bird function for two dimensions is defined as follows \cite{Gavana2016}
\begin{align} \label{eq:Bird} \nonumber
\min f(\bold{x}) =& \left(x_1 - x_2\right)^{2} + e^{\left[1 -
         \sin\left(x_1\right) \right]^{2}} \cos\left(x_2\right) + e^{\left[1 -
          \cos\left(x_2\right)\right]^{2}} \sin\left(x_1\right), \\ 
          &\bold{x} \in [-2\pi, 2\pi]^2  \subset \mathbb{R}^2
\end{align}


The Schwefel01 function for six dimensions is defined as follows \cite{Gavana2016}
\begin{equation} \label{eq:Schwefel01}
\min f(\bold{x})= \left(\sum_{i=1}^n x_i^2 \right)^{\sqrt{\pi}} , ~ \bold{x} \in [-100, 100]^6 \subset \mathbb{R}^6
\end{equation}

\begin{figure} \label{fig:resmin}
\centerline{\includegraphics[scale=0.45]{../7results/img/Fig10.pdf}}
{\caption{(a) The minimiser pool growth of the TGO and SHGO algorithms for the smooth objective function described in Example 3 and restated in \autoref{eq:Ursem01} for convenience, the SHGO never increases above the optimum of $|\mathcal{M}| = 3$, for TGO 3 different values of the $k$ parameter are shown. (b) The minimiser pool growth for the six dimensional Paraboloid problem defined by \autoref{eq:Paraboloid }, note that even though the problem has only one minima the minimiser pool for TGO set at $k = k_c$ tends to increase for increasing sampling points $N$. In general this problem is exacerbated in higher dimensions while SHGO stays at the optimum $|\mathcal{M}| = 1$. The TGO minimiser pool for $k = 3$ and $k = 4$ are not shown here because the minimiser pool grows too rapidly. (c) The minimiser pool growth for the two dimensional Bird problem defined by \autoref{eq:Bird}, an important observation here is that $|\mathcal{M}|$ is higher than optimum for SHGO before the sampling is adequate as defined by \autoref{theorem:invariance_M} which happens at the after there are $N = 1722$ Sobol sequenced points after which $|\mathcal{M}|$ stays at the optimum value equal to the number of unique local minima with increasing $N$. (d) The minimiser pool growth for the six dimensional Schwefel01 problem defined by \autoref{eq:Schwefel01}, here again $|\mathcal{M}|$ for TGO set at $k_c$ grows rapidly with $N$ while $|\mathcal{M}|$ for SHGO stays constant at the optimum.}} 
\label{fig:resmin}
\end{figure}

\subsection{Function evaluations and comparison to other global optimisation algorithms}
The table in Appendix \ref{sec:nres} shows the results for numerical experiments comparing the SHGO and TGO algorithm with 100 sampling points with the SciPy implementation \cite{scipy} of basinhopping (BH) \cite{wales2003energy, wales1997global, li1987monte, wales1999global} and differential evolution (DE) \cite{Storn1997}. These algorithms were chosen both because the open source versions are readily available in the SciPy project and because BH is commonly used in energy surface optimisations \cite{Wales2015} from which the motivation for developing SHGO grew, DE has also been applied in optimising Gibbs energy surfaces for phase equilibria calculations \cite{Zhang2011}. The optimisation problems in Appendix~\ref{sec:nres} were selected from the SciPy global optimisation benchmarking test suite \citep{Gavana2016, Jamil2013, Adorio2005, Mishra2006, Mishra2007, NIST2016} with considerable bias. Most of the more pathological functions were excluded as well as smooth functions with far too many local minima within the given bounds to be mapped with only 100 sampling points and problems with very long runtimes.% Other problems not included are those with a very long runtime which is usually due to constructing the convex hull in the higher dimensions and other unknown errors in the TGO and SHGO code. 

From the results it should first be noted that not all of the 288 starting minimisers produced by SHGO algorithm lead to unique local minima (of which 239 is found across all the problems), this is both due to the fact that the 100 initial sampling points were inadequate for many of the problems and that the local minimisation was not supplied with  the set of bounds provided by the boundary of the $k-$chain around $\textrm{st}\left( v_i \right)$,  $\partial \left( C(\mathcal{H}^k) \right), k = n + 1$, and could therefore jump outside the locally convex domain in the local minimisation step. Of the 301 starting minimisers produced by TGO algorithm only 203 unique minima were found (less than SHGO found and nearly double the total function evaluations).

%In the Table~\ref{tab:results} ndim = $n$ refers the the number of dimensions, nfev refers to the total number of function evaluations used to solved the problem, nlmin refers to the number of local minima found while nulmin refers to the number of local minima that are unique to a set tolerance, success refers to whether or not the algorithm successfully found the correct global minima and finally runtime is the total time the algorithm ran on an optimisation problem measured in seconds.

%\autoref{sec:nres}

The total function evaluations over all problems:
\begin{itemize}
\item \underline{shgo}: 24 406
\item \underline{tgo}: 56 133
\item \underline{differential evolution (de)}: 280 694 
\item \underline{basinhopping (bh)}:  428 035
\end{itemize}

and the overall success rates: 
\begin{itemize}
\label{tab:results}
\item \underline{shgo}: 100.0 \%
\item \underline{tgo}: 97.26 \%
\item \underline{differential evolution (de)}: 87.67 \%
\item \underline{basinhopping (bh)}: 75.34 \%
\end{itemize}



%!! TODO: Lennard-Jones crystals \cite{Chill2014} where we have, using function evaluations as a metric, SHGO was 36.5 times faster than BH, 79.6 times faster than DE and 2.6 times faster than our own in-house version of TGO in solving $n = 6$, simulate for the $n=3 \times 60$ using the symmetry constraints.

%!!TODO: Move to an appendix

\end{document}
