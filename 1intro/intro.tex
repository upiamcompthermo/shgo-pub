\documentclass[../main/shgo.tex]{subfiles}
\begin{document}
\section{Introduction}
\subsection{Objective function statement and nomenclature}
Consider a general non-linear program (NLP) 
\begin{align} \nonumber
\underset{\mathbf{x}}{\textrm{min}} ~&f (\mathbf{x}) \\
\textrm{s.t.}~ &g(\mathbf{x}) \geq 0
\end{align}
The continuous real objective function $f(\mathbf{x})$ of $n$ dimensionality can be either smooth or non-smooth depending on the local minimisation method used. In this publication we mainly consider smooth functions, however, many of the algorithm's properties proved here might be extendable to a wider class of functions.
\begin{equation} \label{eq:ref}
f : \mathbf{\Omega} \subseteq \mathbb{R}^n \rightarrow \mathbb{R}
\end{equation}
with $ \mathbf{x} \in \mathbf{\Omega}  \subseteq \mathbb{R}^n $ where $\mathbf{\Omega}$ is the limited feasible subset excluding points outside the bounds and constraints. $g$ maps the set of non-linear contraints
\begin{equation} \label{eq:ref}
g : \mathbf{\Omega} \subseteq \mathbb{R}^n \rightarrow \mathbb{R}^m
\end{equation}

The variables $\mathbf{x}$ may be bounded or unbounded, for example if lower and upper bounds $l_i$ and $u_i$ are implemented for each variable then we have an initially defined in the hyperrectangle 
\begin{equation}
\mathbf{x} \in \mathbf{\Omega} \subseteq  [l, u]^n~=~[l_1,~u_1]~\times~[l_2,~u_2] ~\times~\dots~\times~[l_n,~u_n] \subseteq  \mathbb{R}^n
\end{equation}

In this publication the $\mathcal{H}$ symbol will be used to represent a simplicial complex rather than the more standard $\Delta $ to avoid confusion with the difference and Laplacian operators common in optimisation. The superscript $\mathcal{H}^k$ represents the subset of $k-$dimensional simplices where for an $n$ dimensional problem the highest dimensional $k-$simplex contains $n + 1$ vertices. $C(\mathcal{H}^k)$ denotes a $k-$chain of $k-$simplices. A vertex in $\mathcal{H}^0$ is denoted by $v_i$. If $v_i$ and $v_j$ are two endpoints of a directed edge in $\mathcal{H}^1$ from $v_i$ to $v_j$ then the symbol $\overline{v_i v_j}$ represents the edge so that it is bounded by the $0-$chain $\partial \left( \overline{v_i v_j} \right) = v_j - v_i$ and similarly for an edge directed from $v_j$ to $v_i$, we have $, \partial \left( \overline{v_j v_i} \right) = \partial \left( - \overline{v_i v_j} \right) = v_i - v_j$. Higher dimensional simplices can be represented and directed in a similar manner, for example a triangle consisting of three vertices $v_i, v_j$ and $v_k$ directed as $\overline{v_i v_j v_k}$  has the boundary of directed edges $\partial \left( \overline{v_i v_j v_j} \right)  = \overline{v_i v_j} + \overline{v_j v_k} + \overline{v_j v_i}$.

\subsection{Multimodal objective functions and local minima mapping}
NLP problems are commonly solved using global optimisation methods. One such example is the topographical global optimisation (TGO) method  \cite{Torn1986, Torn1990, Torn1992, Henderson2015} which is a clustering algorithm that finds several local minima from which the (approximate) global minimum is found. It is often desirable to find all the local minima of the objective function for example in phase equilibria problems where it is required to map the local minima of the Gibbs energy surface to find all the phases in equilibrium using methods described in \citet{Zhang2011} \cite{Michelsen1982, Michelsen1982a, Mitsos2007, Mitsos, Bollas2009, Bollas2009b}.

The graph extracted from the topographical global optimisation (TGO) \cite{Torn1986, Torn1990, Torn1992, Henderson2015} topograph (as described in \autoref{sec:TGO}) is unsatisfactory in some ways. Primarily that several starting points in the same locally convex domain can be generated even when enough information from the objective function sampling is known to prevent this from occurring. This leads to superfluous function evaluations in the local minimisation step of the algorithm. Contrary to intuition, this problem is exacerbated by increasing the number of initial sampling points used in the algorithm as demonstrated in \autoref{sec:TGO}. This can lead to a very large number of function evaluations required to solve the problem. In particular in multimodal energy surfaces where the local minima can often be located in short distances relative to the search space \cite{Zhang2011} and thus requires a large number of initial sampling to locate all these domains. Some shortcomings in using the TGO method to map local minima are:
\begin{itemize}
\item Geometric information available from the sampling points is being discarded by the graphs built up using only the Euclidean distance metric.
\item Knowledge of the number and location of local minimisers in a given sampling set is not being used to the full extent.
\item More than one minimiser might be produced in the same locally convex domain and there is no guarantee that a minimiser set produced by TGO will be in the locally convex domains of all local minima even if the number of local minima is known and a minimiser set of this cardinality is produced.
\end{itemize}

By constructing a directed simplicial complex we show that the simplicial homology global optimisation (SHGO) algorithm never produces superfluous starting points for the class of all Lipschitz smooth functions resulting in more efficient performance for these problems compared to TGO. The directed complex is also used to approximate the homology group of the objective function hypersurface which, using integral homology version of the Invariance Theorem, allows for efficient mapping of optimisation problems where the number of local minima is known \it{a-priori} \normalfont. For example in phase equilibria problems where the maximum number of minima is known through the Gibbs phase rule and in certain problems the exact number of phases is known \it{a-priori} \normalfont \cite{Zhang2011}.
 
\subsection{Overview of this publication}
The TGO method is briefly reviewed in \autoref{sec:TGO} closely following the formalism developed by \citet{Henderson2015}. In \autoref{sec:motivation} we provide a numerical example of TGO which is then used as informal experimental motivations for extending the algorithm. These two sections are important for continuity and understanding of the improved features of SHGO, in particular Definition \autoref{def:optpool} which will be used as a performance criteria. In \autoref{sec:atgo} we present the most immediately apparent extension of TGO and illustrate the shortcomings of that approach. The new SHGO method is then formally presented in \autoref{sec:shgo}. Finally we provide experimental results over a wide class of functions as contained in the SciPy benchmarking test suite in \autoref{sec:results} and conclude with various recommendations for possible further improvements of SHGO.

 
 

\end{document}
